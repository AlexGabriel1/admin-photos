import axios from 'axios';

export const api = axios.create({
	baseURL: 'https://www.usadofacil.com.br/v6',
});
