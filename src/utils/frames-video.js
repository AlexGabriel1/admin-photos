// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
// const functionExtract = async () => {
// 	await extractFrames({
// 		input: '../media/media.mp4',
// 		output: './screenshot-%i.jpg',
// 		offsets: [1000, 2000, 3500],
// 	});
// 	return 'FRAMES EXTRACT';
// };

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const extractFramesFromVideo = (video) => {
	const videoBlobFPS = video;

	return new Promise(async (resolve) => {
		// fully download it first (no buffering):
		console.log('extract frames from video');

		// const fixedURL = 'https://storage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4';

		// let videoBlob = await fetch(fixedURL)
		// 	.then((r) => r.blob())
		// 	.catch((e) => console.log(e));
		let videoObjectUrl = URL.createObjectURL(videoBlobFPS);
		console.log(videoObjectUrl);
		let video = document.createElement('video');

		let seekResolve;
		video.addEventListener('seeked', async function () {
			if (seekResolve) seekResolve();
		});

		video.addEventListener('loadeddata', async function () {
			let canvas = document.createElement('canvas');
			let context = canvas.getContext('2d');
			let [w, h] = [video.videoWidth, video.videoHeight];
			canvas.width = w;
			canvas.height = h;

			let frames = [];
			let interval = 1 / 1;
			let currentTime = 0;
			let duration = 37;

			while (currentTime < duration) {
				video.currentTime = currentTime;
				await new Promise((r) => (seekResolve = r));

				context.drawImage(video, 0, 0, w, h);
				let base64ImageData = canvas.toDataURL();
				frames.push(base64ImageData);

				currentTime += interval;
				console.log(currentTime);
			}

			resolve(frames);
		});

		// set video src 	*after* listening to events in case it loads so fast
		// that the events occur before we were listening.
		video.src = videoObjectUrl;
	});
};

// function downloadBase64File(contentBase64, fileName) {
// 	const linkSource = `data:application/pdf;base64,${contentBase64}`;
// 	const downloadLink = document.createElement('a');
// 	document.body.appendChild(downloadLink);

// 	downloadLink.href = linkSource;
// 	downloadLink.target = '_self';
// 	downloadLink.download = fileName;
// 	downloadLink.click();
// }

export default extractFramesFromVideo;
