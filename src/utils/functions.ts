import imageCompression from 'browser-image-compression';
import Compressor from 'compressorjs';

import { storage } from '../firebase';

export const formatDayString = (): string => {
	const MyDate = new Date();

	const MyDateString = `${
		('0' + MyDate.getDate()).slice(-2) +
		'/' +
		('0' + (MyDate.getMonth() + 1)).slice(-2) +
		'/' +
		MyDate.getFullYear()
	}`;

	return MyDateString;
};

export const formatStringMonth = (mes: number): string | undefined => {
	switch (mes) {
		case 1:
			return 'Janeiro';
		case 2:
			return 'Fevereiro';
		case 3:
			return 'Março';
		case 4:
			return 'Abril';
		case 5:
			return 'Maio';
		case 6:
			return 'Junho';
		case 7:
			return 'Julho';
		case 8:
			return 'Agosto';
		case 9:
			return 'Setembro';
		case 10:
			return 'Outubro';
		case 11:
			return 'Novembro';
		case 12:
			return 'Dezembro';
	}
};

export const yearsRange = (): number[] => {
	let currentYear = new Date().getFullYear();
	const years = [];
	const startYear = 1980;
	while (currentYear >= startYear - 3) {
		years.push(currentYear--);
	}
	return years;
};

export const uploadBase64ArrayToFirebase = async (base64Array: string[], idveiculo: string): Promise<string[]> => {
	try {
		const downloadUrls = [];

		// Itera sobre cada string base64 no array
		for (let i = 0; i < base64Array.length; i++) {
			const base64String = base64Array[i];

			// Converte a string base64 para Blob
			const blob = await base64ToBlob(base64String);

			const compressedBlob = await compressBase64Image_ALT(blob);

			// Cria uma referência para o arquivo no Firebase Storage
			// const storageRef = storage.ref().child(`imagens360/imagem_${i}.jpg`);
			// const storageRef = storage.ref().child(`imagens360/img_1020405_0_0_${i}.jpg`);
			const storageRefCompressed = storage.ref().child(`imagens360/img_${idveiculo}_0_0_${i}.jpg`);

			// Faz o upload do arquivo para o Firebase Storage
			// const snapshot = await storageRef.put(blob);
			const snapshotCompressed = await storageRefCompressed.put(compressedBlob);

			// Adiciona a URL de download do arquivo ao array de URLs
			downloadUrls.push(await snapshotCompressed.ref.getDownloadURL());
		}

		// Retorna o array de URLs de download dentro de uma Promise
		return downloadUrls;
	} catch (error) {
		console.error('Erro ao fazer upload para o Firebase Storage:', error);
		throw error;
	}
};

// Função para converter uma string base64 para Blob
const base64ToBlob = async (base64String: string): Promise<Blob> => {
	const response = await fetch(base64String);
	const blob = await response.blob();
	return blob;
};

// const compressBase64Image = async (blob: Blob): Promise<Blob> => {
// 	return new Promise((resolve, reject) => {
// 		new Compressor(blob, {
// 			quality: 0.6, // Ajuste conforme necessário
// 			maxWidth: 800, // Largura máxima da imagem (opcional)
// 			maxHeight: 600, // Altura máxima da imagem (opcional)
// 			success(result) {
// 				resolve(result);
// 			},
// 			error(error) {
// 				reject(error);
// 			},
// 		});
// 	});
// };

const compressBase64Image_ALT = async (blob: Blob): Promise<Blob> => {
	const options = {
		maxSizeMB: 1, // Exemplo de tamanho máximo de arquivo em MB (ajuste conforme necessário)
		useWebWorker: true, // Opcional: use o web worker para compressão
		fileType: 'image/jpeg', // Opcional: tipo de arquivo de saída
		initialQuality: 0.9, // Opcional: qualidade inicial da compressão (entre 0 e 1)
		alwaysKeepResolution: false, // Opcional: sempre mantenha a resolução original
		// Adicione outras opções conforme necessário
	};

	const file = blobToFile(blob, 'imagem');

	return imageCompression(file, options);
};

// Função para baixar imagens
export const downloadImages = async (urls: string[]): Promise<void> => {
	const storageRef = storage.ref();

	for (const url of urls) {
		try {
			// Obtém a referência do arquivo usando a URL
			const fileRef = storageRef.child(url);

			// Baixa o arquivo
			const downloadURL = await fileRef.getDownloadURL();

			// Use a URL de download, por exemplo, para exibir a imagem em uma página web
			console.log('URL de download:', downloadURL);
		} catch (error) {
			console.error('Erro ao baixar a imagem:', error);
		}
	}
};
const blobToFile = (blob: Blob, fileName: string): File => {
	const file = new File([blob], fileName, { type: blob.type });
	return file;
};
