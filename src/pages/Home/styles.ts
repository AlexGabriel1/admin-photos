import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
	createStyles({
		root: {
			display: 'flex',
		},
		content: {
			flexGrow: 1,
			maxWidth: 1024,
			margin: '0 auto',
		},
		images: {
			width: '100%',
			height: 130,
			border: '2px solid gray',
			padding: 2,
			boxShadow: '1px 1px 1px black',
		},
		image: {
			width: '100%',
			height: 130,
			border: '2px solid gray',
			padding: 2,
			boxShadow: '1px 1px 1px black',
		},
		habilitar360: {
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
			margin: 30,
		},
		actions: {
			marginTop: 50,
		},
		container360loading: {
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			margin: 30,
		},
		saveOrderButton: {
			width: '100%',
			height: 50,
		},
		deletePhotoButton: {
			width: '100%',
			paddingBottom: 10,
			display: 'flex',
		},
		ButtonColor: {
			color: 'crimson',
		},
		ButtonBlur: {
			color: '#9966cc',
		},
		boxThreeButtons: {
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
		},
		colorPrimary: {
			color: '#4caf50',
		},
		Button: {
			color: 'white',
			width: 250,
			marginTop: 50,
			display: 'flex',
			justifyContent: 'center',
			backgroundColor: '#4caf50',
			'&:hover': {
				backgroundColor: '#6fbf73',
			},
		},
		BoxButton: {
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
		},
		center: {
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			margin: 15,
		},
		containerImages360: {
			display: 'flex',
			flexFlow: 'row wrap',
			width: 800,
			justifyContent: 'center',
			alignItems: 'center',
			margin: '0 auto',
			border: '1px solid red',
		},
		image360: {
			width: 180,
			height: 'auto',
		},
	}),
);

export default useStyles;
