/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect, ChangeEvent } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

import { Grid, Button, Typography, Collapse, CircularProgress, Switch } from '@material-ui/core';
import BlurCircular from '@material-ui/icons/BlurCircular';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckIcon from '@mui/icons-material/Check';
import arrayMove from 'array-move';
import imageCompression from 'browser-image-compression';
import { DropzoneArea } from 'material-ui-dropzone';
import qs from 'qs';
import { v4 as uuidv4 } from 'uuid';

import { storage } from '../../firebase';
import { api } from '../../services/api';
import { extractFramesFromVideo } from '../../utils/frames-video';
import { formatStringMonth, uploadBase64ArrayToFirebase, downloadImages } from '../../utils/functions';
import useStyles from './styles';

interface ImagesCar {
	id_foto: number;
	url: string;
	ordem_foto: number;
	nome_foto: string;
}
interface ResponseDetailsCar {
	imagens: ImagesCar[];
	imagem: string;
	Exibir360: boolean;
}
interface ImagesItem {
	index: number;
	urlImage: string;
	nome_foto: string;
	id: number;
}

interface PhotosInterface {
	index: number;
	id: number;
	nome_foto: string;
	urlImage: string;
}
interface ItemPhoto {
	item: PhotosInterface;
}
interface ItemsPhoto {
	items: PhotosInterface[];
}
interface ParamTypes {
	idveiculo: string;
	idanunciante: string;
}

interface UploadResponse {
	sucesso: boolean;
	idveiculo: number;
}

interface ImageMetadata {
	exifData: any; // Tipo pode ser ajustado de acordo com o que você espera receber
	// Outros metadados que deseja extrair
}

const Home: React.FC = () => {
	const { idveiculo, idanunciante } = useParams<ParamTypes>();

	const classes = useStyles();
	const [currentImages, setCurrentImages] = useState<ImagesCar[]>();
	const [items, setItems] = useState<ImagesItem[]>([]);
	const [carImages, setCarImages] = useState<File[]>([]);

	const [saved, setSaved] = useState(false);

	const [videoBlob, setVideoBlob] = useState<Blob | null>(null);
	const [images360, setImages360] = useState<string[]>([]);

	const [isLoading, setIsLoading] = useState(false);

	const [loading360, setLoading360] = useState({
		loading: false,
		success: false,
		error: false,
	});

	const history = useHistory();

	const [checked360, setChecked360] = useState({ anterior: false, novo: false });

	const [deletedItems, setDeletedItems] = useState<number[]>([]);

	const [expanded, setExpanded] = useState({
		adicionar: false,
		ordenar: false,
		deletar: false,
		blur: false,
		view360: false,
	});

	useEffect(() => {
		const DadosVeiculos = {
			id: idveiculo,
			idanunciante: idanunciante,
		};
		const data = JSON.stringify(DadosVeiculos);
		const requestBody = qs.stringify({
			dados: data,
		});

		api.post<ResponseDetailsCar>(`/adm_veiculos_detalhes.asp`, requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				setCurrentImages(response.data.imagens);
				setChecked360({ anterior: response.data.Exibir360, novo: response.data.Exibir360 });
			})
			.catch((response) => {
				console.log(response.data);
			});
	}, []);

	useEffect(() => {
		const fotos = currentImages?.map((item, index) => ({
			index: index,
			id: item.id_foto,
			urlImage: item.url,
			nome_foto: item.nome_foto,
		}));

		let dados = [
			{
				index: 1,
				id: 1,
				urlImage: 'item.url',
				nome_foto: 'item.foto',
			},
		];

		if (fotos !== undefined) {
			dados = fotos;
		}
		setItems(dados);
	}, [currentImages]);

	const handleExpandClick = (comando: number) => {
		if (comando === 1) {
			setExpanded({
				adicionar: true,
				ordenar: false,
				deletar: false,
				blur: false,
				view360: false,
			});
		} else if (comando === 2) {
			setExpanded({
				adicionar: false,
				ordenar: true,
				deletar: false,
				blur: false,
				view360: false,
			});
		} else if (comando === 3) {
			setExpanded({
				adicionar: false,
				ordenar: false,
				deletar: true,
				blur: false,
				view360: false,
			});
		} else if (comando === 4) {
			setExpanded({
				adicionar: false,
				ordenar: false,
				deletar: false,
				blur: true,
				view360: false,
			});
		} else if (comando === 5) {
			setExpanded({
				adicionar: false,
				ordenar: false,
				deletar: false,
				blur: false,
				view360: true,
			});
		}
	};

	const onSortEnd = ({ oldIndex, newIndex }: any) => {
		setItems(arrayMove(items, oldIndex, newIndex));
	};

	const handleButton = () => {
		const idItems = items.map((item, index) => {
			return {
				id: item.id,
				ordem: index + 1,
			};
		});

		const Dados = {
			idanunciante,
			idveiculo,
			fotos: idItems,
		};

		const data = JSON.stringify(Dados);
		const requestBody = qs.stringify({
			dados: data,
		});

		api.post(`/adm_reordena_fotos.asp`, requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					setSaved(true);
					history.go(0);
				}
			})
			.catch((response) => {
				console.log(response.data);
			});
	};

	const handleDeletePhotos = (id_foto: number) => {
		const DadosVeiculos = {
			idveiculo,
			idfoto: id_foto,
		};
		const data = JSON.stringify(DadosVeiculos);

		const requestBody = qs.stringify({
			dados: data,
		});

		api.post(`adm_exclui_fotos.asp`, requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					console.log('sucesso');
					setDeletedItems([...deletedItems, id_foto]);
				}
			})
			.catch(() => {
				console.log('erro');
			});
	};

	const handleBlurPhoto = (url_photo: string) => {
		setIsLoading(true);
		const DadosVeiculos = {
			imagens: [url_photo],
		};
		const data = JSON.stringify(DadosVeiculos);
		// const requestBody = qs.stringify({
		// 	dados: data,
		// });
		api.post(`https://yolov3-licenseplate-api-zb7hsdezma-rj.a.run.app/uploads/`, { imagens: [url_photo] })
			.then((response) => {
				if (response.data) {
					console.log(response.data);
					const url_add = response.data.imagens[0];
					uploadImages(url_add);
				}
			})
			.catch(() => {
				console.log('erro');
			});
	};

	// Adicionar Imagens
	const uploadImages = async (urls: string[]) => {
		const dataUploadImages = {
			idveiculo,
			imagens: urls,
		};

		const data = JSON.stringify(dataUploadImages);

		const requestBody = qs.stringify({
			dados: data,
		});

		api.post('adm_insere_fotos.asp', requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					setIsLoading(false);
					setTimeout(() => {
						history.go(0);
					}, 1000);
				}
			})
			.catch(() => {
				console.log('Erro');
			});
	};

	const handleUploadImg = (url: string) => {
		const DadosVeiculos = {
			id_veiculo: idveiculo,
			imagens: [url],
		};
		const data = JSON.stringify(DadosVeiculos);

		const requestBody = qs.stringify({
			dados: data,
		});

		api.post(`api_360.asp`, requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					console.log('sucesso');
				}
			})
			.catch(() => {
				console.log('erro');
			});
	};

	const handleImagesAdd = (files: File[]) => {
		setCarImages(files);
	};

	const handleImgUpload = async () => {
		setIsLoading(true);
		urlFirebasePromise().then((urlsFirebase: string[]) => {
			uploadImages(urlsFirebase);
		});
	};

	const urlFirebasePromise = async () => {
		return Promise.all(carImages.map((item) => uploadToFirebase(item)));
	};

	async function uploadToFirebase(file: File) {
		//Compressao de imagem
		const imageFile = file;
		const options = {
			maxSizeMB: 0.4,
			maxWidthOrHeight: 1100,
			useWebWorker: true,
			initialQuality: 0.6,
		};
		imageCompression(imageFile, options)
			.then(function (compressedFile) {
				return compressedFile;
			})
			.catch(function (error) {
				console.log(error.message);
			});
		const imageCompressed = imageCompression(imageFile, options);

		// Upload de imagem para o Firebase
		return new Promise<string>(async function (resolve, reject) {
			const carID = uuidv4();
			const date = new Date();
			const month = date.getMonth();

			const StringMonth = formatStringMonth(month + 1);

			const dayString = () => {
				const MyDate = new Date();
				const year = MyDate.getFullYear();
				const MyDay = `${('0' + MyDate.getDate()).slice(-2)}`;
				return { MyDay, year };
			};

			const { MyDay, year } = dayString();
			const day = MyDay;

			const UploadTask = storage
				.ref(`Admin/${StringMonth}-${year}/${day}/ADM-${idveiculo}-${carID}.jpg`)
				.put(await imageCompressed);
			UploadTask.on(
				'state_changed',
				function (snapshot) {
					const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
					console.log(progress);
				},
				function error(err) {
					console.log('error', err);
					reject();
				},
				function complete() {
					UploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
						resolve(downloadURL);
					});
				},
			);
		});
	}

	const handleImages360Add = async (event: ChangeEvent<HTMLInputElement>) => {
		const file = event.target.files?.[0];
		if (!file) return;

		// Verifica se o arquivo é um vídeo MP4
		if (file.type !== 'video/mp4') {
			alert('Por favor, selecione um vídeo MP4');
			return;
		}

		// Lê o arquivo como um Blob
		const reader = new FileReader();
		reader.onload = async () => {
			const result = reader.result;
			if (result instanceof ArrayBuffer) {
				const blobVideo = new Blob([result], { type: 'video/mp4' });
				// Agora, usamos blobVideo em vez de videoBlob
				setVideoBlob(blobVideo);
			}
		};
		reader.readAsArrayBuffer(file);
	};

	const url360 = async (firebaseUrls: string[]) => {
		return Promise.all(firebaseUrls.map((item) => handleUploadImg360(item)));
	};

	const handleButton360 = async () => {
		setLoading360({
			success: false,
			loading: true,
			error: loading360.error,
		});

		if (videoBlob) {
			const arrayString64 = await extractFramesFromVideo(videoBlob);
			console.log(arrayString64);
			setImages360(arrayString64);
			const firebaseUrls = await uploadBase64ArrayToFirebase(arrayString64, idveiculo);
			console.log(firebaseUrls);
			const returnPromisseUrl360 = await url360(firebaseUrls);
			console.log(returnPromisseUrl360);

			const requestCheck = returnPromisseUrl360.filter((item) => item.sucesso !== true);

			if (requestCheck.length == 0) {
				setLoading360({
					success: true,
					loading: false,
					error: false,
				});
			} else {
				setLoading360({
					success: false,
					loading: false,
					error: true,
				});
			}
		} else {
			const DadosVeiculos = {
				id_veiculo: idveiculo,
				imagens: [''],
				habilita360: checked360.novo,
				ignoraAtualizacao: checked360.anterior === checked360.novo ? true : false,
			};
			const data = JSON.stringify(DadosVeiculos);

			const requestBody = qs.stringify({
				dados: data,
			});

			api.post(`api_360.asp`, requestBody, {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
			}).then((response) => {
				if (response.data.sucesso === true) {
					setLoading360({
						success: true,
						loading: false,
						error: false,
					});
				}
			});
		}
	};

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setChecked360({ anterior: checked360.anterior, novo: event.target.checked });
	};

	const handleUploadImg360 = async (url: string): Promise<UploadResponse> => {
		const DadosVeiculos = {
			id_veiculo: idveiculo,
			imagens: [url],
			habilita360: checked360.novo,
			ignoraAtualizacao: checked360.anterior === checked360.novo ? true : false,
		};
		const data = JSON.stringify(DadosVeiculos);

		const requestBody = qs.stringify({
			dados: data,
		});

		const request = await api.post(`api_360.asp`, requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		});

		return request.data;
	};

	const SortableItem = SortableElement(({ item }: ItemPhoto) => (
		<Grid item xs={3} sm={2} md={2}>
			<img src={`${item.urlImage}`} alt="Imagem" className={classes.images} />
		</Grid>
	));

	const SortableList = SortableContainer(({ items }: ItemsPhoto) => (
		<Grid container spacing={2}>
			{items.map((item, index) => (
				<SortableItem key={`${item.id}`} index={index} item={item} />
			))}
		</Grid>
	));

	return (
		<div className={classes.root}>
			<main className={classes.content}>
				<div>
					{currentImages && (
						<Grid container spacing={2}>
							{currentImages?.map((item, index) => (
								<Grid key={index} item sm={2} xs={2} md={2}>
									<img
										key={index}
										alt="Imagem veículo"
										src={`${item.url}`}
										className={classes.image}
									/>
									<div style={{ textAlign: 'center' }}>{item.nome_foto}</div>
								</Grid>
							))}
						</Grid>
					)}
				</div>

				<div className={classes.boxThreeButtons}>
					<Button
						id="Button"
						onClick={() => handleExpandClick(1)}
						variant="text"
						size="large"
						className={classes.Button}
					>
						Adicionar Imagens
					</Button>
					<Button
						id="Button2"
						onClick={() => handleExpandClick(2)}
						variant="contained"
						size="large"
						className={classes.Button}
					>
						Ordenar Imagens
					</Button>
					<Button
						id="Button3"
						onClick={() => handleExpandClick(3)}
						variant="contained"
						size="large"
						className={classes.Button}
					>
						Deletar Imagens
					</Button>
					<Button
						id="Button3"
						onClick={() => handleExpandClick(4)}
						variant="contained"
						size="large"
						className={classes.Button}
					>
						Ocultar Placas
					</Button>
					<Button
						id="Button5"
						onClick={() => handleExpandClick(5)}
						variant="contained"
						size="large"
						className={classes.Button}
					>
						360º
					</Button>
				</div>

				<Collapse in={expanded.adicionar} timeout="auto" unmountOnExit>
					<div className={classes.center}>
						<Typography variant="h4" component="h6">
							Adicionar fotos
						</Typography>
					</div>

					<DropzoneArea
						key={40}
						acceptedFiles={['image/*']}
						dropzoneText={'Arraste ou clique para inserir imagens '}
						onChange={(files) => handleImagesAdd(files)}
						filesLimit={40}
						showPreviewsInDropzone={true}
						showPreviews={false}
						previewText="Imagens"
						showAlerts={false}
						clearOnUnmount={true}
					/>

					<div className={classes.BoxButton}>
						<Button
							id="Button"
							onClick={handleImgUpload}
							variant="contained"
							disabled={isLoading}
							size="large"
							className={classes.Button}
						>
							{isLoading ? <CircularProgress className={classes.colorPrimary} /> : 'Adicionar Imagens'}
						</Button>
					</div>
				</Collapse>

				<Collapse in={expanded.ordenar} timeout="auto" unmountOnExit>
					<div className={classes.center}>
						<Typography variant="h4" component="h6">
							Ordenar Fotos
						</Typography>
					</div>

					<SortableList items={items} onSortEnd={onSortEnd} axis="xy" helperClass="Sortable" />

					<div className={classes.BoxButton}>
						<Button
							id="Button"
							onClick={handleButton}
							variant="contained"
							size="large"
							className={classes.Button}
						>
							Salvar ordem das imagens
						</Button>
					</div>

					{saved && (
						<div className={classes.BoxButton}>
							<p>Salvo com sucesso!</p>
						</div>
					)}
				</Collapse>

				<Collapse in={expanded.deletar} timeout="auto" unmountOnExit>
					<div className={classes.center}>
						<Typography variant="h4" component="h6">
							Deletar Fotos
						</Typography>
					</div>

					{currentImages && (
						<Grid container spacing={3}>
							{currentImages?.map((item, index) => (
								<Grid key={index} item sm={4} xs={6} md={2}>
									<img
										key={index}
										src={item.url}
										alt={`imagem - ${index}`}
										className={classes.image}
									/>
									<Button
										onClick={() => handleDeletePhotos(item.id_foto)}
										disabled={deletedItems.includes(item.id_foto)}
										className={classes.deletePhotoButton}
									>
										{deletedItems.includes(item.id_foto) ? (
											<DeleteIcon />
										) : (
											<DeleteIcon className={classes.ButtonColor} />
										)}
									</Button>
								</Grid>
							))}
						</Grid>
					)}
				</Collapse>

				<Collapse in={expanded.blur} timeout="auto" unmountOnExit>
					<div className={classes.center}>
						<Typography variant="h4" component="h6">
							Fotos
						</Typography>
					</div>

					{currentImages && (
						<Grid container spacing={3}>
							{currentImages?.map((item, index) => (
								<Grid key={index} item sm={4} xs={6} md={2}>
									<img
										key={index}
										src={item.url}
										alt={`imagem - ${index}`}
										className={classes.image}
									/>
									<Button
										onClick={() => handleBlurPhoto(item.url)}
										disabled={isLoading}
										className={classes.deletePhotoButton}
									>
										{isLoading ? <BlurCircular /> : <BlurCircular className={classes.ButtonBlur} />}
									</Button>
								</Grid>
							))}
						</Grid>
					)}
				</Collapse>

				<Collapse in={expanded.view360} timeout="auto" unmountOnExit>
					<div className={classes.center}>
						<Typography variant="h4" component="h6">
							Adicionar 360
						</Typography>
					</div>

					<div className={classes.habilitar360}>
						<Typography color="textPrimary" variant="h6" component="h6">
							Habilitar 360
						</Typography>
						<Switch
							checked={checked360.novo}
							onChange={handleChange}
							inputProps={{ 'aria-label': 'controlled' }}
						/>
					</div>

					<div className={classes.habilitar360}>
						<input type="file" accept="video/mp4" onChange={handleImages360Add} />
						<span>Adicione um arquivo de vídeo apenas se for gerar o componente pela primeira vez.</span>
					</div>

					{/* <div className={classes.containerImages360}>
						{images360.map((base64String, index) => (
							<img key={index} className={classes.image360} src={base64String} alt={`Imagem ${index}`} />
						))}
					</div> */}
					{loading360.loading && (
						<div className={classes.container360loading}>
							<CircularProgress className={classes.colorPrimary} />
							<Typography color="primary" variant="h6" component="h6">
								Processando e Gerando imagens, aguarde...
							</Typography>
						</div>
					)}
					{loading360.success && (
						<div className={classes.container360loading}>
							<CheckIcon style={{ color: 'Green' }} />
							<Typography style={{ color: 'Green' }} variant="h6" component="h6">
								Alterações realizadas com Sucesso!
							</Typography>
						</div>
					)}
					{loading360.error && (
						<div className={classes.container360loading}>
							<Typography style={{ color: 'red' }} variant="h6" component="h6">
								Erro na geraçao do compoennete, tente novamente.
							</Typography>
						</div>
					)}

					<div className={classes.BoxButton}>
						<Button
							id="Button"
							onClick={handleButton360}
							variant="contained"
							disabled={loading360.loading || loading360.success}
							size="large"
							className={classes.Button}
						>
							Salvar
						</Button>
					</div>

					{/* {videoBlob && (
						<video controls width="400">
							<source src={URL.createObjectURL(videoBlob)} type="video/mp4" />
							Seu navegador não suporta vídeo HTML5.
						</video>
					)} */}
				</Collapse>
			</main>
		</div>
	);
};

export default Home;
