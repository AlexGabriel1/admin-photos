import React from 'react';
import { Route, HashRouter } from 'react-router-dom';

import Home from './pages/Home';

const Routes: React.FC = () => {
	return (
		<HashRouter>
			<Route path="/:idveiculo/:idanunciante">
				<Home />
			</Route>
		</HashRouter>
	);
};

export default Routes;
