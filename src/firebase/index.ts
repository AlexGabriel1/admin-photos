import firebase from 'firebase/app';
import 'firebase/storage';

// Usado Facil Lojistas
// const firebaseConfig = {
// 	apiKey: 'AIzaSyADW_TxMhdoOyJ4hQDpX787L3DxOG6OzPA',
// 	authDomain: 'usado-facil-lojistas.firebaseapp.com',
// 	databaseURL: 'https://usado-facil-lojistas.firebaseio.com',
// 	projectId: 'usado-facil-lojistas',
// 	storageBucket: 'usado-facil-lojistas.appspot.com',
// 	messagingSenderId: '818461621369',
// 	appId: '1:818461621369:web:3b5d9362c2f7d9daa7a8a8',
// 	measurementId: 'G-QJXJ5XD74L',
// };

//UsadoFacil LojistasWeb
const firebaseConfig = {
	apiKey: 'AIzaSyBlkHyaxSWhmEJ47WDDjAg3l0kyvc33dUI',
	authDomain: 'usadofacil-lojistasweb.firebaseapp.com',
	databaseURL: 'https://usadofacil-lojistasweb.firebaseio.com',
	projectId: 'usadofacil-lojistasweb',
	storageBucket: 'usadofacil-lojistasweb.appspot.com',
	messagingSenderId: '314426145004',
	appId: '1:314426145004:web:367f724cce62ec1f914dc9',
	measurementId: 'G-KNYG1M2WMS',
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };
